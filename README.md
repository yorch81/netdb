# netDb #

## Description ##
Abstract DataBase Layer for SQL Server, MySQL and ODBC in C#. 

## Requirements ##
* [.NET Framework](http://www.microsoft.com/es-mx/download/details.aspx?id=30653)
* [Mono](http://www.mono-project.com/)
* [SQL Server](http://www.microsoft.com/es-es/server-cloud/products/sql-server/)
* [MySQL](http://www.mysql.com/)
* [ODBC](http://support.microsoft.com/kb/110093)

## Developer Documentation ##
In the Code.

## Installation ##
Add nuget reference: 
	PM> Install-Package netDb.dll

## Notes ##
SQL Server, MySQL and ODBC Support.

## Example ##
~~~
netDb.netDb _conn = netDb.netDb.GetInstance(netDb.netDb.MSSQLSERVER, serverDb, userDb, password, dbName);

if (_conn.IsConnected ()) {
	string query = "SELECT * FROM TABLE WHERE field = @param";

	DataSet ds = _conn.Execute (query, _conn.GetParameter ("@param", "parameter value"));
} 
~~~

## References ##
http://es.wikipedia.org/wiki/Singleton




