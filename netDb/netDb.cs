﻿using System.Data.Common;
using System.Data;

// netDb  
//
// netDb Singleton Implementation Class
//
// Copyright 2015 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace netDb
{
    /// <summary>
    /// Singleton Implementation Class
    /// </summary>
    public class netDb
    {
        /// <summary>
        /// Available Implementations
        /// </summary>
        public const int MSSQLSERVER = 1;
        public const int MYSQL = 2;
        public const int ODBC = 3;

        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static netDb INSTANCE = null;

        /// <summary>
        /// Abstract Connection
        /// </summary>
        private absLayer conn = null;

        /// <summary>
        /// Private Constructor
        /// </summary>
        /// <param name="connType">
        /// 1 MSSQLSERVER 2 MYSQL 3 ODBC
        /// </param>
        /// <param name="server">
        /// Server Name
        /// </param>
        /// <param name="user">
        /// User Name
        /// </param>
        /// <param name="password">
        /// Password User
        /// </param>
        /// <param name="dbName">
        /// DataBase Name
        /// </param>
		/// <param name="port">
		/// DataBase Port
		/// </param>
		private netDb(int connType, string server, string user, string password, string dbName, int port)
        {
			// Configure Log File
			Logger.ConfigureFileAppender ("netdb.log");

            switch (connType)
            {
                case 1:
                    conn = new absSqlServer(server, user, password, dbName, port);
                    break;
                case 2:
                    conn = new absMySql(server, user, password, dbName, port);
                    break;
                case 3:
                    conn = new absOdbc(server, user, password);
                    break;
                default:
                    conn = new absSqlServer(server, user, password, dbName, port);
                    break;
            }
        }

        /// <summary>
        /// Singleton Implementation
        /// </summary>
        /// <param name="connType">
        /// 1 MSSQLSERVER 2 MYSQL 3 ODBC
        /// </param>
        /// <param name="server">
        /// Server Name
        /// </param>
        /// <param name="user">
        /// User Name
        /// </param>
        /// <param name="password">
        /// Password User
        /// </param>
        /// <param name="dbName">
        /// DataBase Name
        /// </param>
        /// <returns>
        /// Singleton Instance
        /// </returns>
        /// <remarks>
        /// For ODBC Server is the DSN Name
        /// </remarks>
		public static netDb GetInstance(int connType = 1, string server = "", string user = "", string password = "", string dbName = "", int port = 1433)
        {
            if (INSTANCE == null)
            {
				INSTANCE = new netDb(connType, server, user, password, dbName, port);
            }

            return INSTANCE;
        }

        /// <summary>
        /// Checks if Connected to DataBase Server
        /// </summary>
        /// <returns>
        /// bool
        /// </returns>
        public bool IsConnected()
        {
            return conn.IsConnected();
        }

        /// <summary>
        /// Execute query without result
        /// </summary>
        /// <param name="query">
        /// Query to execute
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// -2 Not Connected -1 SQL Exception 0 No Results in execution >0 Rows Affected
        /// </returns>
        public int ExecuteNonQuery(string query, params DbParameter[] p)
        {
            return conn.ExecuteNonQuery(query, p);
        }

        /// <summary>
        /// Execute query with result
        /// </summary>
        /// <param name="query">
        /// Query to execute
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// Dataset
        /// </returns>
        public DataSet Execute(string query, params DbParameter[] p)
        {
            return conn.Execute(query, p);
        }

        /// <summary>
        /// Returns a Parameter
        /// </summary>
        /// <param name="name">
        /// Parameter Name
        /// </param>
        /// <param name="value">
        /// Parameter Value
        /// </param>
        /// <returns>
        /// DbParameter
        /// </returns>
        /// <remarks>
        /// For ODBC the parameters in query must be ? 
        /// and the parameter name does not matter
        /// </remarks>
        public DbParameter GetParameter(string name, object value)
        {
            return conn.GetParameter(name, value);
        }

        /// <summary>
        /// Returns First Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet First(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            return conn.First(table, fieldKey, key, where);
        }

        /// <summary>
        /// Returns Next Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet Next(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            return conn.Next(table, fieldKey, key, where);
        }

        /// <summary>
        /// Returns Previous Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet Previous(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            return conn.Previous(table, fieldKey, key, where);
        }

        /// <summary>
        /// Returns Last Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet Last(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            return conn.Last(table, fieldKey, key, where);
        }

        /// <summary>
        /// Gets a DataSet from Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="limit">
        /// Limit of Rows
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet Table(string table, int limit = 1000)
        {
            return conn.Table(table, limit);
        }

        /// <summary>
        /// Gets a DataSet from Execution of Procedure
        /// </summary>
        /// <param name="procedure">
        /// Stored Procedure Name
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet Procedure(string procedure, params DbParameter[] p)
        {
            return conn.Procedure(procedure, p);
        }

        /// <summary>
        /// Gets Row of Table for Id
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet TableById(string table, string fieldKey = "id", int key = 0)
        {
            return conn.TableById(table, fieldKey, key);
        }
    }
}
