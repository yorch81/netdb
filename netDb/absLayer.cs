﻿using System.Data.Common;
using System.Data;

// netDb 
//
// netDb Abstract DataBase Layer Class
//
// Copyright 2015 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace netDb
{
    /// <summary>
    /// Abstract DataBase Layer Class
    /// </summary>
    abstract class absLayer
    {
        /// <summary>
        /// Checks if Connected to DataBase Server
        /// </summary>
        /// <returns>
        /// bool
        /// </returns>
        public abstract bool IsConnected();

        /// <summary>
        /// Execute query without result
        /// </summary>
        /// <param name="query">
        /// Query to execute
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// -2 Not Connected -1 SQL Exception 0 No Results in execution >0 Rows Affected
        /// </returns>
        public abstract int ExecuteNonQuery(string query, params DbParameter[] p);

        /// <summary>
        /// Execute query with result
        /// </summary>
        /// <param name="query">
        /// Query to execute
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// Dataset
        /// </returns>
        public abstract DataSet Execute(string query, params DbParameter[] p);

        /// <summary>
        /// Returns a Parameter
        /// </summary>
        /// <param name="name">
        /// Parameter Name
        /// </param>
        /// <param name="value">
        /// Parameter Value
        /// </param>
        /// <returns>
        /// DbParameter
        /// </returns>
        public abstract DbParameter GetParameter(string name, object value);

        /// <summary>
        /// Returns First Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public abstract DataSet First(string table, string fieldKey = "id", int key = 0, string where = "");

        /// <summary>
        /// Returns Next Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public abstract DataSet Next(string table, string fieldKey = "id", int key = 0, string where = "");

        /// <summary>
        /// Returns Previous Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public abstract DataSet Previous(string table, string fieldKey = "id", int key = 0, string where = "");

        /// <summary>
        /// Returns Last Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public abstract DataSet Last(string table, string fieldKey = "id", int key = 0, string where = "");

        /// <summary>
        /// Gets a DataSet from Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="limit">
        /// Limit of Rows
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public abstract DataSet Table(string table, int limit = 1000);

        /// <summary>
        /// Gets a DataSet from Execution of Procedure
        /// </summary>
        /// <param name="procedure">
        /// Stored Procedure Name
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public abstract DataSet Procedure(string procedure, params DbParameter[] p);

        /// <summary>
        /// Gets a Datable with Error Information
        /// </summary>
        /// <param name="errorCode">
        /// Error Code
        /// </param>
        /// <param name="errormsg">
        /// Error Message
        /// </param>
        /// <returns>
        /// DataTable
        /// </returns>
        protected DataTable GetErrorTable(int errorCode, string errormsg)
        {
            DataTable errorTable = new DataTable();

            errorTable.Columns.Add("errorcode", typeof(int));
            errorTable.Columns.Add("errormsg", typeof(string));

            errorTable.Rows.Add(errorCode, errormsg);

            return errorTable;
        }

        /// <summary>
        /// Gets Row of Table for Id
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public abstract DataSet TableById(string table, string fieldKey = "id", int key = 0);
    }
}
