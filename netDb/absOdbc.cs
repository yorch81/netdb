﻿using System;
using System.Data.Odbc;
using System.Data;
using System.Data.Common;
using log4net;
using System.Text;

// absOdbc  
//
// absOdbc ODBC Implementation Class
//
// Copyright 2015 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace netDb
{
    /// <summary>
    /// ODBC Implementation Class
    /// </summary>
	class absOdbc : absLayer
    {
		/// <summary>
		/// Logger Object.
		/// </summary>
		private static readonly ILog logger = LogManager.GetLogger(typeof(absOdbc));

        /// <summary>
        /// Odbc Connection
        /// </summary>
        OdbcConnection connection = null;

        /// <summary>
        /// Constructor of class
        /// </summary>
        /// <param name="server">
        /// ODBC DSN Name
        /// </param>
        /// <param name="user">
        /// ODBC User
        /// </param>
        /// <param name="password">
        /// ODBC Password
        /// </param>
        public absOdbc(string server, string user, string password)
        {
            connection = new OdbcConnection();
            connection.ConnectionString = "dsn=" + server + ";UID=" + user + ";PWD=" + password + ";";

            try
            {
                connection.Open();

                if (!(connection.State == ConnectionState.Open))
                {
                    connection = null;
                }
            }
            catch (Exception e)
            {
				logger.Error (e.Message);

                connection = null;
            }
        }

        /// <summary>
        /// Checks if Connected to Odbc DSN
        /// </summary>
        /// <returns>
        /// bool
        /// </returns>
        public override bool IsConnected()
        {
            return connection == null ? false : true;
        }

        /// <summary>
        /// Execute query without result
        /// </summary>
        /// <param name="query">
        /// Query to execute
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// -2 Not Connected -1 ODBC SQL Exception 0 No Results in execution >0 Rows Affected
        /// </returns>
        public override int ExecuteNonQuery(string query, params DbParameter[] p)
        {
            int retValue = 0;

            if (IsConnected())
            {
                try
                {
                    OdbcCommand myCommand = new OdbcCommand(query, connection);

                    for (var i = 0; i < p.Length; i++)
                    {
                        myCommand.Parameters.Add(p[i]);
                    }

                    retValue = myCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
					logger.Error (e.Message);
                    retValue = -1;
                }
            }
            else
                retValue = -2;

            return retValue;
        }

        /// <summary>
        /// Execute query with result
        /// </summary>
        /// <param name="query">
        /// Query to execute
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// Dataset
        /// </returns>
        public override DataSet Execute(string query, params DbParameter[] p)
        {
            DataSet myDs = null;

            if (IsConnected())
            {
                try
                {
                    OdbcCommand myCommand = new OdbcCommand(query, connection);
                    OdbcDataAdapter myAdapter = new OdbcDataAdapter();
                    myDs = new DataSet();

                    for (var i = 0; i < p.Length; i++)
                    {
                        myCommand.Parameters.Add(p[i]);
                    }

                    myAdapter.SelectCommand = myCommand;
                    myAdapter.Fill(myDs);
                }
                catch (Exception e)
                {
					logger.Error (e.Message);

                    myDs = new DataSet();
                    myDs.Tables.Add(GetErrorTable(-1, e.Message));
                }
            }
            else
            {
                myDs = new DataSet();
                myDs.Tables.Add(GetErrorTable(-2, "Not Connected to ODBC DSN"));
            }

            return myDs;
        }

        /// <summary>
        /// Returns a ODBC Parameter
        /// </summary>
        /// <param name="name">
        /// Parameter Name
        /// </param>
        /// <param name="value">
        /// Parameter Value
        /// </param>
        /// <returns>
        /// DbParameter
        /// </returns>
        public override DbParameter GetParameter(string name, object value)
        {
            return new OdbcParameter(name, value);
        }

        /// <summary>
        /// Returns First Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet First(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            DataSet retValue = new DataSet();

            retValue.Tables.Add(GetErrorTable(-1, "Not Supported for ODBC"));

            return retValue;
        }

        /// <summary>
        /// Returns Next Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet Next(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            DataSet retValue = new DataSet();

            retValue.Tables.Add(GetErrorTable(-1, "Not Supported for ODBC"));

            return retValue;
        }

        /// <summary>
        /// Returns Previous Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet Previous(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            DataSet retValue = new DataSet();

            retValue.Tables.Add(GetErrorTable(-1, "Not Supported for ODBC"));

            return retValue;
        }

        /// <summary>
        /// Returns Last Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet Last(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            DataSet retValue = new DataSet();

            retValue.Tables.Add(GetErrorTable(-1, "Not Supported for ODBC"));

            return retValue;
        }

        /// <summary>
        /// Gets a DataSet from Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="limit">
        /// Limit of Rows
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet Table(string table, int limit = 1000)
        {
            DataSet retValue = new DataSet();

            retValue.Tables.Add(GetErrorTable(-1, "Not Supported for ODBC"));

            return retValue;
        }

        /// <summary>
        /// Gets a DataSet from Execution of Procedure
        /// </summary>
        /// <param name="procedure">
        /// Stored Procedure Name
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet Procedure(string procedure, params DbParameter[] p)
        {
            DataSet retValue = new DataSet();

            retValue.Tables.Add(GetErrorTable(-1, "Not Supported for ODBC"));

            return retValue;
        }

        /// <summary>
        /// Gets Row of Table for Id
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet TableById(string table, string fieldKey = "id", int key = 0)
        {
            DataSet retValue = new DataSet();

            StringBuilder sQuery = new StringBuilder("SELECT * FROM ");
            sQuery.Append(table);
            sQuery.Append(" WHERE ");
            sQuery.Append(fieldKey);
            sQuery.Append(" = ");
            sQuery.Append(key);
            
            retValue = this.Execute(sQuery.ToString());

            sQuery = null;

            return retValue;
        }
    }
}
