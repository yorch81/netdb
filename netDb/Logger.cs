﻿using System;
using log4net.Config;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using log4net.Appender;
using System.Text;
using log4net.Core;
using log4net;

// Logger 
//
// Logger configure Log File
//
// Copyright 2015 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace netDb
{
	/// <summary>
	/// configure Log File.
	/// </summary>
	class Logger
	{
		/// <summary>
		/// Configures the file appender.
		/// </summary>
		/// <param name="logFile">Log file.</param>
		public static void ConfigureFileAppender(string logFile)
		{
			var fileAppender = GetFileAppender( logFile );

			BasicConfigurator.Configure( fileAppender );
			( ( Hierarchy ) LogManager.GetRepository() ).Root.Level = Level.Debug;
		}

		/// <summary>
		/// Gets the file appender.
		/// </summary>
		/// <returns>The file appender.</returns>
		/// <param name="logFile">Log file.</param>
		private static IAppender GetFileAppender(string logFile)
		{
			var layout = new PatternLayout( "%date %-5level %logger - %message%newline" );
			layout.ActivateOptions(); // According to the docs this must be called as soon as any properties have been changed.

			var appender = new FileAppender
			{
				File = logFile,
				Encoding = Encoding.UTF8,
				Threshold = Level.Debug,
				Layout = layout
			};

			appender.ActivateOptions();

			return appender;
		}
	}
}

