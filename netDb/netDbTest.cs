using System;
using NUnit.Framework;
using System.Data;

// netDbTest  
//
// netDbTest Test for netDb
//
// Copyright 2015 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace netDb
{
	/// <summary>
	/// NetDbTest Class.
	/// </summary>
	[TestFixture ()]
	public class netDbTest
	{
		/// <summary>
		/// netDb Instance
		/// </summary>
		private netDb _conn;

		/// <summary>
		/// Initializes netDb Instance
		/// </summary>
		public netDbTest ()
		{
			// Change Settings
			String serverDb = "localhost";
			String userDb = "sa";
			String password = "password";
			String dbName = "db";

			_conn = netDb.GetInstance(netDb.MSSQLSERVER, serverDb, userDb, password, dbName, 1433);
		}

		/// <summary>
		/// Test a single query.
		/// </summary>
		[Test]
		public void queryTest()
		{
			if (_conn.IsConnected()) {
				string query = "SELECT 1 AS FIELD WHERE 1 = @param";

				DataSet ds = _conn.Execute (query, _conn.GetParameter ("@param", 1));

				int result = (int) ds.Tables[0].Rows[0]["FIELD"];

				// if result = 1
				Assert.AreEqual (1, result);
			} else
				Assert.AreEqual (true, false);
		}
	}
}

